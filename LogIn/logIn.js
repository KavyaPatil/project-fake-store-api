
const firstNameInput = document.querySelector("#first-name");
const lastNameInput = document.querySelector("#last-name");
const passwordInput = document.querySelector("#password");
const repeatPasswordInput = document.querySelector("#repeat-password");
const mailInput = document.querySelector("#mail");
const checkboxInput = document.querySelector("#checkbox");

const firstName = firstNameInput.value.trim();
const lastName = lastNameInput.value.trim();
const password = passwordInput.value.trim();
const repeatPassword = repeatPasswordInput.value.trim();
const mail = mailInput.value.trim();

const nameWarn = document.querySelector("#name-warn");
const lastWarn = document.querySelector("#last-warn");
const mailWarn = document.querySelector("#mail-warn");
const passWarn = document.querySelector("#pass-warn");
const confirmWarn = document.querySelector("#confirm-warn");
const termsWarn = document.querySelector("#terms-warn");


var inputField = document.getElementById("myInput");
var errorDiv = document.getElementById("error");

document.getElementById("form").addEventListener("submit", function(event) {
  event.preventDefault(); // Prevent form submission

  // var input = inputField.value;
  const firstName = firstNameInput.value.trim();
const lastName = lastNameInput.value.trim();
const password = passwordInput.value.trim();
const repeatPassword = repeatPasswordInput.value.trim();
const mail = mailInput.value.trim();

  if (firstName === "") {
    showError(nameWarn, "Enter Name");
  }
  else if (!isValid(firstName)) {
    showError(nameWarn, "Name must include only alphabets");
  }  else {
    clearError(nameWarn);
  }

    if (lastName === "") {
      showError(lastWarn, "Enter Last Name.");
    } else if (!isValid(lastName)) {
      showError(lastWarn, "Last Name must include only alphabets");
    } else {
      clearError(lastWarn);
    }

    if (mail === "") {
      showError(mailWarn, "Enter Email");
    } else if (!isValidEmail(mail)) {
      showError(mailWarn, "Email must include  mail@gmail.com");
    } else {
      clearError(mailWarn);
    }

    if (password === "") {
      showError(passWarn, "Enter password.");
    }else if (password.length < 8) {
      showError(passWarn, "Password must have 8 characters");
    }  else {
      clearError(passWarn);
    }

    if (repeatPassword === "") {
      showError(confirmWarn, "Confirm password.");
    } else if (repeatPassword !== password) {
      showError(confirmWarn, "Passwords must match");
    } else {
      clearError(confirmWarn);
    }

    if (!checkboxInput.checked) {
      console.log(checkboxInput,checkboxInput.value, checkboxInput.value.checked)
      showError(termsWarn, "Agree to the terms and conditions");
    }  else {
      clearError(termsWarn);
    }
    // Input is not empty and valid, continue with form submission or other actions
    // ...
    
    if(nameWarn.innerText === "" && lastWarn.innerText === "" && passWarn.innerText === "" && confirmWarn.innerText === "" && mailWarn.innerText === "" && termsWarn.innerText === ""){
      console.log('submitted');
      window.location.href= "https://project-fake-store-api.vercel.app";
    }
    // document.getElementById("form").submit();
  
});

firstNameInput.addEventListener("input", function() {
  const input = firstNameInput.value;
console.log(firstNameInput, input)
  if (input.trim() !== "") {
    clearError(nameWarn);
  }
});

lastNameInput.addEventListener("input", function() {
  const input = lastNameInput.value;

  if (input.trim() !== "") {
    clearError(lastWarn);
  }
});

mailInput.addEventListener("input", function() {
  const input = mailInput.value;

  if (input.trim() !== "" && isValidEmail(input)) {
    clearError(mailWarn);
  }
});

passwordInput.addEventListener("input", function() {
  const input = passwordInput.value;

  if (input.trim() !== "") {
    clearError(passWarn);
  }
});

repeatPasswordInput.addEventListener("input", function() {
  const input = repeatPasswordInput.value;

  if (input.trim() !== "" && passwordInput.value === repeatPasswordInput.value) {
    clearError(confirmWarn);
  }
});

checkboxInput.addEventListener("change", function(event) {
  const input = event.target.checked;

  if (input) {
    clearError(termsWarn);
  }
});

function showError(errorDiv, message) {
  errorDiv.style.color = "red";
  errorDiv.innerText = message;
}

function clearError(errorDiv) {
  errorDiv.innerText = "";
}

function isValid(input) {

  const lettersRegex = /^[A-Za-z]+$/;
  return lettersRegex.test(input);
}


function isValidEmail(email) {
  // Simple email validation regex pattern
  var emailPattern = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
  return emailPattern.test(email);
}

