const container = document.querySelector(".container");
const electronics = document.querySelector(".electronic");
const jewelery = document.querySelector(".jewelery");
const men = document.querySelector(".men");
const women = document.querySelector(".women");
const errElement = document.querySelector(".error");
const loader = document.querySelector(".lds-ring");
// container.innerHTML = "";
const electronicsButton = document.querySelector("#electronics");
const jeweleryButton = document.querySelector("#jewelery");
const menButton = document.querySelector("#men");
const womenButton = document.querySelector("#women");

console.log(electronics);
loader.style.display = "inline-block";


function fetchData(contain, url) {


  fetch(url)
    .then((res) => res.json())
    .then((json) => {
      // json.length = 0;
      if(json.length === 0){
        document.querySelector("#no-product").textContent = "No Products..";
        return;
      }
      console.log(contain);
      // contain.innerHTML = json.reduce((accumulator, item) => {
      //   // console.log(item.rating)
      //   accumulator += `
      //             <div class="tile" id="${item.id}">
      //             <div class="flex">
      //                 <div><img src="${item.image}" class="image"></div>
      //                 <div class="details">
      //                 <div class="blue"> ${item.title}</div>
      //                 <div class="category-text gray"> ${item.description.slice(0, 150)}...</div>
      //                 </div>
      //                 </div>
      //             <div>
      //             <div class="category-text">category: ${item.category}</div>
      //             <div class="flex justify-between ">
      //             <div class="price"> Price: $ ${item.price}</div>
      //             <div>${getStars(item.rating.rate)}|${item.rating.count} votes</div>
      //             </div>

      //             </div>
      //              </div> `;

      //              return accumulator;
      // }, '');

      json.forEach((item) => {
        // Create the main container element
        const tileDiv = document.createElement("div");
        tileDiv.classList.add("tile");
        tileDiv.id = item.id;

        // Create the inner flex container
        const flexDiv = document.createElement("div");
        flexDiv.classList.add("flex");

        // Create the image element
        const imageElement = document.createElement("img");
        imageElement.src = item.image;
        imageElement.classList.add("image");
        const imageDiv = document.createElement("div");
        imageDiv.appendChild(imageElement);
        flexDiv.appendChild(imageDiv);

        // Create the details container
        const detailsDiv = document.createElement("div");
        detailsDiv.classList.add("details");

        // Create the title element
        const titleDiv = document.createElement("div");
        titleDiv.classList.add("blue");
        titleDiv.textContent = item.title;
        detailsDiv.appendChild(titleDiv);

        // Create the description element
        const descriptionDiv = document.createElement("div");
        descriptionDiv.classList.add("category-text", "gray");
        descriptionDiv.textContent = item.description.slice(0, 150) + "...";
        detailsDiv.appendChild(descriptionDiv);

        flexDiv.appendChild(detailsDiv);

        tileDiv.appendChild(flexDiv);

        // Create the category element
        const categoryDiv = document.createElement("div");
        categoryDiv.classList.add("category-text");
        categoryDiv.textContent = "category: " + item.category;
        tileDiv.appendChild(categoryDiv);

        // Create the price and rating elements
        const priceDiv = document.createElement("div");
        priceDiv.classList.add("price");
        priceDiv.textContent = "Price: $" + item.price;

        const ratingDiv = document.createElement("div");
        ratingDiv.innerHTML = getStars(item.rating.rate) + "|" + item.rating.count + " votes";

        const flexJustifyDiv = document.createElement("div");
        flexJustifyDiv.classList.add("flex", "justify-between");
        flexJustifyDiv.appendChild(priceDiv);
        flexJustifyDiv.appendChild(ratingDiv);

        tileDiv.appendChild(flexJustifyDiv);

        // Append the tile element to the document or a parent container
        contain.appendChild(tileDiv);
        loader.style.display = "none";

      });
    }).catch(err =>{
      console.log(err);
      window.location.href= "https://project-fake-store-api.vercel.app/pageNotFound.html";
    // errElement.innerHTML = "Having Problem with fetching the data, refresh the page..." + "/n" + "https://project-fake-store-api.vercel.app/"
    })
}

fetchData(container, "https://fakestoreapi.com/products");

fetchData(electronics, "https://fakestoreapi.com/products/category/electronics");

fetchData(jewelery, "https://fakestoreapi.com/products/category/jewelery");

fetchData(men, "https://fakestoreapi.com/products/category/men's clothing");

fetchData(women, "https://fakestoreapi.com/products/category/women's clothing");

electronicsButton.addEventListener("click", () => {
  container.style.display = "none";
  jewelery.style.display = "none";
  men.style.display = "none";
  women.style.display = "none";
  electronics.style.display = "flex";
});

jeweleryButton.addEventListener("click", () => {
  container.style.display = "none";
  jewelery.style.display = "flex";
  men.style.display = "none";
  women.style.display = "none";
  electronics.style.display = "none";
});

menButton.addEventListener("click", () => {
  container.style.display = "none";
  jewelery.style.display = "none";
  men.style.display = "flex";
  women.style.display = "none";
  electronics.style.display = "none";
});

womenButton.addEventListener("click", () => {
  container.style.display = "none";
  jewelery.style.display = "none";
  men.style.display = "none";
  women.style.display = "flex";
  electronics.style.display = "none";
});

function getStars(rating) {
  // Round to nearest half
  rating = Math.round(rating * 2) / 2;
  let output = [];

  // Append all the filled whole stars
  for (var index = rating; index >= 1; index--) {
    output.push(
      '<i class="fa fa-star" aria-hidden="true" style="color: #4A55A2;"></i>&nbsp;'
    );
  }

  // If there is a half a star, append it
  if (index == 0.5) {
    output.push(
      '<i class="fa fa-star-half-o" aria-hidden="true" style="color: #4A55A2;"></i>&nbsp;'
    );
  }
  // Fill the empty stars
  for (let index = 5 - rating; index >= 1; index--) {
    output.push(
      '<i class="fa fa-star-o" aria-hidden="true" style="color: #4A55A2;"></i>&nbsp;'
    );
  }
  return output.join("");
}
